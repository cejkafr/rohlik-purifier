//var pattern = "https://mdn.mozillademos.org/*";
var pattern = "https://www.rohlik.cz/services/frontend-service/categories/*/metadata";

function onCompleted(details) {

//	console.log("Completed: " + details.url);

	let filter = browser.webRequest.filterResponseData(details.requestId);
	let decoder = new TextDecoder("utf-8");
	let encoder = new TextEncoder();

	filter.ondata = event => { 
		let json = JSON.parse(decoder.decode(event.data, {stream: true}));

//		console.log(json);

		json.data.informationBlocks = [];
		json.data.additionalInformation = null;
		json.data.banner = null;
		json.data.productSections = [];
		json.data.upsellProducts = {};

		filter.write(encoder.encode(JSON.stringify(json)))
		filter.disconnect();
	}

	return {};

}

browser.webRequest.onBeforeRequest.addListener(
	onCompleted,
	{ urls: [pattern], types: ["xmlhttprequest"] },
	["blocking"]
);
