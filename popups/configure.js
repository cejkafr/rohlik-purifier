const options = ["sexy", "intro", "banner", "upsell", "story"]

function elementClicked(e) {
	let id = e.target.id;

	if (!isInOptions(id)) {
		return;
	}

	let checked = e.target.childNodes[0].checked

	console.log("click " + id + " " + checked);

  	var getting = browser.storage.sync.get("settings");
  	getting.then(result => {
		console.log(result);
//        	browser.storage.sync.set({
//        	        "param" + id: checked
//	        });
	}, onError);
}

function loadSettings(e) {

	console.log("load " + e);

}

function isInOptions(id) {
	for (var i in options) {
		if (id == "opt-" + options[i]) {
			return true;
		}
	}

	return false;
}

function onError(error) {
	console.log(error);
}

document.addEventListener("click", elementClicked);
document.addEventListener("DOMContentLoaded", loadSettings);
